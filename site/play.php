<?php

require_once('common.inc');
require_once('actions.inc');

$table = $_REQUEST['table'];
if ($table == null) {
    http_response_code(400);
    die("Missing table ID!");
}

session_id($table);
session_start();

$player = (isset($_REQUEST['player'])) ? $_REQUEST['player'] : null;
$key = (isset($_REQUEST['key'])) ? $_REQUEST['key'] : null;

$queue = msg_get_queue(crc32($table));

function publish($queue, $message) {
    foreach ($_SESSION['players'] as $color => $player) {
        msg_send($queue, crc32($player['key']), $message, false);
    }
    msg_send($queue, 1, $message, false);
}

if ($player != null && $key == $_SESSION['players'][$player]['key']) {
    $action = match ($_REQUEST['action']) {
        'message'  => action_message($_REQUEST['text']),
        'end_turn' => action_end_turn(),
        'arrow'    => action_arrow($_REQUEST['from'], $_REQUEST['dir']),
        'field'    => action_field($_REQUEST['id']),
        'reset'    => action_reset(),
        'end'      => action_end()
    };
    publish($queue, action($action));

    if ($_REQUEST['action'] == 'end_turn') {
        $action = action_turn(false);
        publish($queue, action($action));
    }
    
    file_put_contents("${table}.save", json_encode($_SESSION));
}

?>
