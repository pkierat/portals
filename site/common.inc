<?php

ini_set('session.cookie_samesite', 'Strict');

const COLORS = array('red', 'orange', 'yellow', 'green', 'blue', 'violet');

function guidv4($data = null) {
    $data = $data ?? random_bytes(16);
    assert(strlen($data) == 16);

    $data[6] = chr(ord($data[6]) & 0x0f | 0x40);
    $data[8] = chr(ord($data[8]) & 0x3f | 0x80);

    return vsprintf('%s%s-%s-%s-%s-%s%s%s', str_split(bin2hex($data), 4));
}

function dice() {
    return match (rand(1, 8)) {
        1, 2, 3 => 'BUILD',
        4, 5, 6 => 'MOVE',
        7 => 'START',
        8 => 'CENTER',
    };
}

?>
