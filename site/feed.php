<?php

const TIMEOUT = 1000 * 1000 * 60 * 60; // 60 minutes
const INTERVAL = 10000;                // 10 milliseconds

if ($_REQUEST['table']) {
    $table = $_REQUEST['table'];
} else {
    http_response_code(400);
    die("Missing table ID!");
}

session_id($table);
session_start();

$key = (isset($_REQUEST['key'])) ? $_REQUEST['key'] : null;
$queue = msg_get_queue(crc32($table));
$msg_type = $key ? crc32($key) : 1;

header('Content-Type: text/event-stream');
header('Cache-Control: no-cache');
header('X-Accel-Buffering: no');

foreach ($_SESSION['actions'] as $action) {
    echo "data: {$action}\n\n";
}

ob_flush();
flush();

session_write_close();

function send($msg) {
    echo "data: {$msg}\n\n";
    ob_flush();
    flush();
}

$timeout = TIMEOUT;
while (!connection_aborted()) {
    if (msg_receive($queue, $msg_type, $rcv_type, 256, $msg, false, MSG_IPC_NOWAIT)) {
        send($msg);
    } else if ($timeout > 0) {
        if ($timeout % 1000000 == 0) {
            send("{}");  // keep-alive
        }
        usleep(INTERVAL);
        $timeout -= INTERVAL;
    } else {
        break;
    }
}

?>
