<?php
$players = $_SESSION['players'];

header('Content-Type: text/html');
header('Cache-Control: no-cache');

function joined($player) {
    return isset($player['key']);
}

?>
<!DOCTYPE html>
<html>
<head>
    <title>Portals</title>
    <meta charset="utf-8">
    <meta http-equiv="Cache-Control" content="no-cache, no-store, must-revalidate">
    <meta http-equiv="Pragma" content="no-cache">
    <meta http-equiv="Expires" content="0">
    <?php if (!$_SESSION['started']) { ?><meta http-equiv="refresh" content="5"><?php } ?>
</head>
<body>
    <h1>Welcome to P⏣rtals!</h1>
    <h2>Table ID: <?=$table?> (<a href='/join.php?table=<?=$table?>' target='_blank'>New join link</a>)</h2>
    <p>
    <label for="players"><strong>Players:</strong></label>
    <table id="players">
        <tbody>
        <?php foreach ($_SESSION['players'] as $color => $pinfo) { ?>
            <tr>
                <td style="color: <?=$color?>">■</td>
                <td><?php if(isset($pinfo['name'])) { ?>
                        <?=$pinfo['name']?> (<a href='/join.php?table=<?=$table?>&player=<?=$color?>&key=<?=$pinfo['key']?>'>link</a>)
                    <?php } else { ?>
                        [empty]
                    <?php } ?>
                </td>
            </tr>
        <?php } ?>
        </tbody>
    </table>
    </p>
    <?php if (count(array_filter($_SESSION['players'], 'joined')) < count($_SESSION['players'])) { ?>
    <form action="/start.php" method="get">
        <input type="hidden" name="table" value="<?=$table?>">
        <input type="submit" value="Refresh!" autofocus>
    </form>
    <?php } else { ?>
    <form action="start.php" method="post" target="_blank">
        <input type="checkbox" id="dice_mode" name="dice_mode" value="true" title="If set, actions will be chosen randomly. If not, the decision will belong to the current player.">
        <label for="dice_mode">Dice mode</label><br><br>
        <input type="hidden" name="table" value="<?=$table?>">
        <input type="hidden" name="start" value="true">
        <input type="submit" value="Start!" autofocus>
    </form>
    <?php } ?>
</body>
</html>
