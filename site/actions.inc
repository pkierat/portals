<?php

function action($action) {
    $action['id'] = count($_SESSION['actions']);
    $action['origin'] = sender();
    $action['timestamp'] = now();
    $message = json_encode($action);
    array_push($_SESSION['actions'], $message);
    return $message;
}

function action_message($text) {
    return array('action' => 'message', 'args' => array($text));
}

function action_player($start, $color) {
    return array('action' => 'player', 'args' => array($start, $color));
}

function action_start() {
    return array('action' => 'start', 'args' => array());
}

function action_turn($first) {
    if (!$first) { next_player(); }
    return array(
        'action' => 'turn',
        'args' => array(current_player(), $_SESSION['dice_mode'] ? dice() : null)
    );
}

function action_arrow($from, $dir) {
    return array( 'action' => 'arrow', 'args' => array($from, $dir));
}

function action_field($id) {
    return array( 'action' => 'field', 'args' => array($id));
}

function action_reset() {
    return array('action' => 'reset', 'args' => array());
}

function action_end_turn() {
    return array('action' => 'end_turn', 'args' => array());
}

function action_end() {
    return array('action' => 'end', 'args' => array());
}

function current_player() {
    return $_SESSION['players_queue'][0];
}

function next_player() {
    array_push($_SESSION['players_queue'], array_shift($_SESSION['players_queue']));
}

function sender() {
    if (isset($_REQUEST['key'])) {
        foreach ($_SESSION['players'] as $color => $player) {
            if (isset($player['key']) && $player['key'] == $_REQUEST['key']) {
                return $color;
            }
        }
    }
    return null;
}

function now() {
    return date("Y-m-d\TH:i:s.vp");
}

?>
