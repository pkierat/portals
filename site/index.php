<?php

if (isset($_REQUEST['table'])) {
    $table = $_REQUEST['table'];
    session_id($table);
    session_start();
} else {
    $table = null;
}

header('Content-Type: text/html');
header('Cache-Control: no-cache');

if (isset($_REQUEST['key'])) {
    $_SESSION['last'][$_REQUEST['key']] = 0;
}

?>
<!DOCTYPE html>
<html>
    <head>
        <title>Portals</title>
        <meta charset="UTF-8"/>
        <link rel="stylesheet" href="portals.css"/>
        <script type="text/javascript" src="js/Elements.js"></script>
        <script type="text/javascript" src="js/Player.js"></script>
        <script type="text/javascript" src="js/Board.js"></script>
        <script type="text/javascript" src="js/Action.js"></script>
        <script type="text/javascript" src="js/state/State.js"></script>
        <script type="text/javascript" src="js/state/Setup.js"></script>
        <script type="text/javascript" src="js/state/Play.js"></script>
        <script type="text/javascript" src="js/state/DicePlay.js"></script>
        <script type="text/javascript" src="js/state/GameOver.js"></script>
        <script type="text/javascript" src="js/state/StateDecorator.js"></script>
        <script type="text/javascript" src="js/state/LoggedPlay.js"></script>
        <script type="text/javascript" src="js/controller/GameServer.js"></script>
        <script type="text/javascript" src="js/controller/GameController.js"></script>
        <script type="text/javascript" src="js/Config.js"></script>
        <script type="text/javascript" src="js/Chat.js"></script>
        <script type="text/javascript" src="js/UI.js"></script>
        <script type="text/javascript" src="js/Game.js"></script>
        <script type="text/javascript" src="js/event.js"></script>
        <script type="text/javascript" src="js/api.js"></script>
        <script type="text/javascript" src="js/portals.js"></script>
    </head>
    <body>
       <div id="game">
           <div id="logo">P⏣rtals</div>
           <div id="config">
               <?php if ($table == null) { ?>
               <form id="start" action="start.php" method="post" target="_blank">
                   <input type="hidden" name="players">
                   <button id="start_local" disabled>Hot Seat</button>
                   <button id="start_online" disabled>Online</button>
               </form>
               <?php } ?>
           </div>
           <div id="rules" align="right">Rules: [<a href="portals-en.html">EN</a> | <a href="portals-pl.html">PL</a>]</div>
           <div id="tips">
               <label for="instructions"><h3>Instructions</h3></label>
               <div id="instructions">
                   <span id="setup">
                   <strong>Setup</strong>
                   <p>Place a start platform for each player:
                      <ul>
                      <li>Click on an empty corner field.</li>
                      <li>Click again to change its color.</li>
                      </ul>
                      Once done, start the game:
                      <ul>
                      <li><strong>Hot Seat</strong> to play locally.</li>
                      <li><strong>Online</strong> to play over the Internet.</li>
                      </ul>
                      Online game:
                      <ol>
                       <li>Right-click on <strong>New join link</strong>, copy it and send it to all players.</li>
                       <li>Open it in a new tab for yourself, if you also want to play.</li>
                       <li>Wait for all players to join.</li>
                       <li>Click <strong>Start</strong> to start the game.</li>
                      </ol> 
                   </p>
                   </span>
                   <span id="goal">
                   <strong>Goal</strong>
                   <p>Take at least one your pawn off the Center and get rid of your remaining pawns.</p>
                   </span>
                   <span id="play">
                   <strong>Play</strong>
                   <p>Upon your turn, do one of the following:
                   <ul><li>
                       Build a piece of road (<a href="/portals-en.html#_building_a_road" title="more...">❔</a>):
                       <ul><li>Click on an arrow to add a platform or a portal.</li>
                           <li>(Optional) Click more arrows to add connections.</li>
                       </ul>
                   <li>Jump through a portal (<a href="/portals-en.html#_portal_activation" title="more...">❔</a>):
                       <ul><li>Click on an active portal.</li></ul>
                   </li>
                   <li>Place a new pawn (<a href="/portals-en.html#_introducing_a_pawn_to_start" title="more...">❔</a>):
                       <ul><li>Click on your Start platform.</li></ul>
                   </li>
                   <li>Take your pawn off the Center (<a href="/portals-en.html#_removing_a_pawn_from_center" title="more...">❔</a>):
                       <ul><li>Click on the pawn.</li></ul></li>
                   </ul>
                   Once done, click <strong>End Turn</strong>.<br>
                   To undo, click <strong>Reset Turn</strong>.
                   </p>
                   </span>
               </div>
           </div>
           <div id="main" align="center">
               <iframe id="_board_" src="board.svg" style="visibility:hidden"></iframe>
               <div id="controls" align="center">
                   <button id="end_turn" disabled>End Turn</button>
                   <button id="reset"    disabled>Reset Turn</button>
               </div>
           </div>
           <div id="chat">
               <label for="messages"><h3>Chat</h3></label>
               <textarea id="messages" readonly></textarea>
           </div>
           <div id="empty1"></div>
           <form id="prompt">
                <input id="message" type="text" placeholder="Write a message..."/>
                <input id="send" type="submit" value="Send!"/>
           </form>
           <div id="empty2">&nbsp;</div>
       </div>
    </body>
</html>
