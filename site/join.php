<?php

require_once('common.inc');

$table = $_REQUEST['table'];
if ($table == null) {
    http_response_code(400);
    die("Missing game ID!");
}

session_id($table);
session_start();

$players = $_SESSION['players'];
$player = isset($_REQUEST['player']) ? $_REQUEST['player'] : "";
$key = isset($_REQUEST['key']) ? $_REQUEST['key'] : "";

if ($_SESSION['started']) {
    header("Location: /index.php?table={$table}&player={$player}&key=${key}", true, 302);
    exit();
}

header('Content-Type: text/html');
header('Cache-Control: no-cache');

if ($_POST) {

    if (isset($players[$player]) && !isset($players[$player]['key'])) {
        $players[$player]['key'] = $key = guidv4();
        $players[$player]['name'] = $_POST['name'];
    }
    $_SESSION['players'] = $players;

    header( "Location: {$_SERVER['REQUEST_URI']}?table={$table}&player={$player}&key={$key}", true, 303);

} else {

    require_once('join.form.inc');

} ?>
