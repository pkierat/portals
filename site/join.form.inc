<!DOCTYPE html>
<html>
<head>
    <title>Portals</title>
    <meta charset="utf-8">
    <meta http-equiv="refresh" content="5">
</head>
<body>
    <h1>Welcome to P⏣rtals!</h1>
    <h2>Table ID: <?=$table?></h2>
    <p>
    <label for="players"><strong>Players:</strong></label>
    <table id="players">
        <tbody>
        <?php foreach ($players as $color => $pinfo) { ?>
            <tr>
                <td style="color: <?=$color?>">■</td>
                <td>
                <?php if (isset($pinfo['key'])) { ?>
                    <?=$pinfo['name']?><?=$pinfo['key'] == $key ? ' (you)' : ""?>
                <?php } else if ($key == "") { ?>
                    <form action="/join.php" method="post">
                        <input type="hidden" name="table" value="<?=$table?>">
                        <input type="hidden" name="player" value="<?=$color?>">
                        <input type="text" name="name" placeholder="Your name" autofocus>
                        <input type="submit" value="Join!">
                    </form>
                <?php } else { ?>
                    [empty]
                <?php } ?>
                </td>
            </tr>
        <?php } ?>
        </tbody>
    </table>        
    </p>
    <form action="/join.php" method="get">
        <input type="hidden" name="table" value="<?=$table?>">
        <input type="hidden" name="player" value="<?=$player?>">
        <input type="hidden" name="key" value="<?=$key?>">
        <input type="submit" value="Refresh!">
    </form>
</body>
</html>
