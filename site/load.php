<?php

require_once('common.inc');

const UUID_REGEX = "^[0-9a-f]{8}\b-[0-9a-f]{4}\b-[0-9a-f]{4}\b-[0-9a-f]{4}\b-[0-9a-f]{12}$";

$error = "";

if ($_POST) {

    if (isset($_POST['table'])) {
        $table = $_POST['table'];
        if (!preg_match("/".UUID_REGEX."/", $table)) {
            $error = "Invalid table ID!";
        } else {
            $filename = "${table}.save";
            if (!file_exists($filename)) {
                $error = "No game found for this table ID!";
            } else {
                $loaded = json_decode(file_get_contents($filename), true);

                session_id($table);
                session_start();

                foreach ($loaded as $key => $value) {
                    $_SESSION[$key] = $value;
                }

                header("Location: /start.php?table={$table}", true, 303);
                exit();
            }
        }
    } else {
        $error = "Table ID cannot be empty!";
    }
} ?>
<!DOCTYPE html>
<html>
<head>
    <title>Portals</title>
    <meta charset="utf-8">
</head>
<body>
    <h1>Welcome to P⏣rtals!</h1>
    <form action="/load.php" method="post">
        <label for="table">Table ID:</label><br>
        <input type="text" id="table" name="table" autofocus><br>
        <span id="error" style="color: red;"><?=$error?></span><br><br>
        <input type="submit" value="Load!">
    </form>
</body>
</html>
