<?php

require_once('common.inc');
require_once('actions.inc');

if (isset($_REQUEST['table'])) {
    $table = $_REQUEST['table'];
} else {
    $table = guidv4();
}

session_id($table);
session_start();

function new_game() {
    $_SESSION['started'] = false;
    $_SESSION['players'] = array();
    $_SESSION['actions'] = array();
    $_SESSION['players_queue'] = array();
    $_SESSION['last'] = array();
    $_SESSION['dice_mode'] = false;
    error_log($_REQUEST['players']);
    if (isset($_REQUEST['players'])) {
        $_players = array();
        parse_str($_REQUEST['players'], $_players);
        foreach ($_players as $color => $start) {
            $_SESSION['players'][$color] = array('start' => $start);
            array_push($_SESSION['players_queue'], $color);
            action(action_player($start, $color));
        }
    }
}

function start_game() {
}

if (!isset($_SESSION['players'])) {
    new_game();
}

if ($_POST) {

    if (isset($_POST['start'])) {
        $_SESSION['dice_mode'] = isset($_POST['dice_mode']);
        action(action_start());
        action(action_turn(true));
        $_SESSION['started'] = true;
        $key = guidv4();
        header("Location: /index.php?table={$table}&key={$key}", true, 303);
    } else {
        header("Location: /start.php?table={$table}", true, 303);
    }

} else {

    require_once('start.form.inc');

}
?>
