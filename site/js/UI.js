class UI {

    gameElement;
    controlsElement;
    endTurn;
    resetTurn;

    constructor(gameElement, controlsElement, handler) {
        this.gameElement = gameElement;
        this.controlsElement = controlsElement;
        
        this.endTurn = this.controlsElement.querySelector("#end_turn");
        this.resetTurn = this.controlsElement.querySelector("#reset");
        this.endTurn.onclick = (e) => handler.endTurn();
        this.resetTurn.onclick = (e) => handler.resetTurn();
    }

    lock() {
        this.gameElement.dataset.locked = true;
    }

    unlock() {
        delete this.gameElement.dataset.locked;
    }

    message(msg) {
        alert(msg);
    }

    enableEndTurn(enabled) {
        this.endTurn.disabled = !enabled;
    }

    enableResetTurn(enabled) {
        this.resetTurn.disabled = !enabled;
    }

    enableControls(enabled) {
        this.controlsElement.querySelectorAll("input, button")
            .forEach((el) => { el.disabled = !enabled });
    }

}
