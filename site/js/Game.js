var game;

class Game {
  
    element;
    board;
    config;
    ui;
    chat;
    logger;

    controller;

    current;
    players;

    #state;
    #turn;

    constructor(element, board, config, ui, chat, logger) {
        this.element = element;
        this.board = board;
        this.config = config;
        this.ui = ui;
        this.chat = chat;
        this.logger = logger;

        this.controller = new GameController(this, this.config.online);

        this.board.game = this;

        this.state = new Setup(this);

        this.ui.enableEndTurn(false);
        this.ui.enableResetTurn(false);
    }

    get state() { return this.#state; }
    set state(val) {
        this.#state = val;
        this.element.dataset.state = val;
    }

    get action() { return this.element.dataset.action; }
    set action(val) {
        if (val == undefined) {
            delete this.element.dataset.action;
        } else {
            this.element.dataset.action = val;
        }
    }

    get turn() { return this.#turn; }
    set turn(val) { this.#turn = val; this.element.dataset.turn = val; }

    get currentPlayer() {
        return this.current;
    }

    enableStart() {
        this.config.enable(true);
        this.action = "start";
    }

    start() {
        this.players = this.board.getPlayers();
        this.action = undefined;
        this.board.save();
        this.config.enable(false);
        this.ui.enableControls(true);
        if (this.config.options.dice) {
            this.state = new DicePlay(this);
        } else {
            this.state = new Play(this);
        }
        if (this.config.online) {
            this.element.dataset.player = this.config.options.player;
        }
        this.turn = 0;
        return this.state.onGameStart();
    }

    end() {
        this.state.onGameEnd();
        this.state = new GameOver(this);
        return false;
    }

    endTurn() {
        if (!this.state.onTurnEnd(true)) {
            return false;
        }
        this.ui.enableControls(false);
        this.checkWinner(this.currentPlayer);
        return this.players.length > 0 || this.onGameEnd();
    }

    nextTurn(player = undefined, action = undefined) {
        this.turn++;
        this.nextPlayer(player);
        this.board.save();
        this.ui.enableControls(this.isLocalPlayersTurn());
        return this.onTurnStart(action);
    }

    nextPlayer(player) {
        if (!player) {
            player = game.players.shift();
        } else {
            this.removePlayer(player);
        }
        this.players.push(player);
        
        this.current = player;
        this.board.player = this.current;
    }

    isLocalPlayersTurn() {
        return !this.config.online
            || (this.config.viewOnly
                && this.config.options.player == this.currentPlayer);
    }

    removePlayer(player) {
        this.players = this.players.filter(p => p != player);
        return player;
    }

    resetTurn() {
        if (this.state.onTurnReset(true)) {
            this.board.restore();
            return true;
        }
        return false;
    }

    checkWinner() {
        let player = this.currentPlayer;
        if (this.board.isCenterReached(player)) {
            player.phase = "1";
            if (!this.board.hasToken(player)) {
                player.phase = "2";
                this.removePlayer(player);
                alert(`${player} wins!`);
            }
        }
    }

    toPlayer(color) {
        return this.current != null && this.current.color == color
            ? this.current
            : this.players.find((p) => p.color == color);
    }

    toAction(action) {
        if (!action) { return undefined; }
        let result = undefined;
        switch (action) {
            case 'CENTER':
                result = new Finish(game);
                if (result.isAllowed()) break;
            case 'START':  result = new Start(this); break;
            case 'MOVE':   result = new Move(this);  break;
            case 'BUILD':  result = new Build(this); break;
            default: result = new Done(this);
        }
        return result;
    }

    ////////////////// Events //////////////////

    onTurnStart(action) {
        if (action != undefined && this.isLocalPlayersTurn()) {
            alert("Action: " + action.toString().toUpperCase());
        }
        return this.state.onTurnStart(action);
    }

    onFieldClick(field) {
        return this.controller.field(field.id);
    }

    onArrowClick(arrow) {
        return this.controller.arrow(arrow.source.id, arrow.dir.toString());
    }

    onTurnReset() {
        return this.controller.reset();
    }

    onTurnEnd() {
        return this.controller.end_turn();
    }

    onWin(player) {
        return this.controller.win(player.color);
    }

    onGameEnd() {
        return this.controller.end();
    }

}
