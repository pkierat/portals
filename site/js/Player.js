class Player {

    element;
    color;

    constructor(element) {
        this.element = element;
        this.color = element.dataset.player;
    }

    get start() { return this.element.id; }

    get phase() { return this.element.dataset.phase; }
    set phase(phase) { this.element.dataset.phase = phase; }

    toString() {
        return this.color;
    }

}
