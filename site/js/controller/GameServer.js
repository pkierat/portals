class GameServer {

    game;

    constructor(game) {
        this.game = game;
    }

    send(action, data = {}) { }

    receive(data) {
        switch (data.action) {
            case 'player': {
                let f = this.game.board.fieldAt(data.args[0]);
                return f.toStart(data.args[1]) != undefined;
            }
            case 'start': {
                return this.game.turn === undefined
                    && this.game.start();
            }
            case 'turn': {
                return this.game.nextTurn(
                    data.args[0] ? this.game.toPlayer(data.args[0]) : undefined,
                    data.args[1] ? this.game.toAction(data.args[1]) : undefined
                );
            }
            case 'field': {
                let f = this.game.board.fieldAt(data.args[0]);
                return this.game.state.onFieldClick(f, true);
            }
            case 'arrow': {
                let f = this.game.board.fieldAt(data.args[0]);
                return this.game.state.onArrowClick(f.arrow(new Coords(data.args[1])), true);
            }
            case 'reset': {
                return this.game.resetTurn(true);
            }
            case 'end_turn': {
                return this.game.endTurn();
            }
            case 'win': {
                return true;
            }
            case 'end': {
                this.game.ui.message("Game over!");
                this.disconnect();
                return this.game.end();
            }
            case 'message': {
                return this.game.chat.message(data.timestamp, data.origin, data.args[0]);
            }
            default: return false;
        }
    }

    connect(options) {
    }
    
    disconnect() {
    }

}

class LocalServer extends GameServer {

    constructor(game) {
        super(game);
    }

    send(action, data) {
        let player = this.game.current;
        let timestamp = new Date().toJSON();
        this.receive({action: action, origin: player, timestamp: timestamp, args: Object.values(data)});
    }

    receive(data) {
        switch (data.action) {
            case 'end_turn': return super.receive(data) && this.game.nextTurn();
            default: super.receive(data);
        }
    }

}

class RemoteServer extends GameServer {

    options;
    eventSource;
    lastId;

    constructor(game) {
        super(game);
        this.lastId = -1;
    }

    connect(options) {
        this.options = options;
        let feedUrl = "/feed.php?" + new URLSearchParams(options);
        this.eventSource = new EventSource(feedUrl);
        this.eventSource.onmessage = (evt) => { this.receive(JSON.parse(evt.data)) };
    }

    disconnect() {
        this.eventSource.close();
    }

    send(action, data = {}) {
        data.action = action;
        data.table = this.options.table;
        data.player = this.options.player;
        data.key = this.options.key;
        data.timestamp = new Date().toJSON();
        fetch(`/play.php`, {
            method: "POST",
            headers: { 'Content-type': 'application/x-www-form-urlencoded' },
            body: new URLSearchParams(data).toString()
        });
        return true;
    }

    receive(data) {
        if (data.id == undefined) { return false; }
        if (data.id > this.lastId) {
            this.lastId = data.id;
            return super.receive(data);
        }
    }

}
