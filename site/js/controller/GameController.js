class GameController {

    game;
    server;

    constructor(game, online) {
        this.game = game;
        this.server = online
            ? new RemoteServer(game)
            : new LocalServer(game);
        this.server.connect(game.config.options);
    }

    player(fieldId, color) {
        return this.server.send('player', {id: fieldId, color: color});
    }
    
    start() {
        return this.game.turn === undefined && this.server.send('start', {});
    }
    
    turn(player, action) {
        return this.server.send('turn', {player: player, action: action});
    }
    
    field(id) {
        let field = this.game.board.fieldAt(id);
        return this.game.state.onFieldClick(field, false)
            && this.server.send('field', {id: id});
    }
    
    arrow(fieldId, dir) {
        let field = this.game.board.fieldAt(fieldId);
        return this.game.state.onArrowClick(field.arrow(new Coords(dir)), false)
            && this.server.send('arrow', {from: fieldId, dir: dir});
    }
    
    end_turn() {
        if (!this.game.state.onTurnEnd(false)) {
            this.game.ui.message("You have an action to take!");
            return false;
        }
        return this.server.send('end_turn', {});
    }
    
    reset() {
        return this.game.resetTurn(false)
            && this.server.send('reset', {});
    }

    win(player) {
        return this.server.send('win', {player: player});
    }
    
    end() {
        return this.server.send('end', {});
    }

    message(text) {
        return text != "" && this.server.send('message', {text: text});
    }

}

class LoggingGameController extends GameController {

    constructor(game, online, logger) {
        super(game, online);
        this.logger = logger;
    }

    player(fieldId, color) {
        return super.player(fieldId, color)
            && this.logger.log(`player('${fieldId}', '${color}')`);
    }
    
    start() {
        return super.start()
            && this.logger.log(`start()`);
    }
    
    turn(player, action) {
        return super.turn(player, action)
            && this.logger.log(`turn('${player}', '${action}')`);
    }
    
    field(id) {
        return super.field(id)
            && this.logger.log(`field('id')`);
    }
    
    arrow(fieldId, dir) {
        return super.arrow(fieldId, dir)
            && this.logger.log(`arrow('${fieldId}, '${dir}')`);
    }
    
    end_turn() {
        return super.end_turn()
            && this.logger.log(`end_turn()`);
    }
    
    reset() {
        return super.reset()
            && this.logger.log(`reset()`);
    }

    win(player) {
        return super.win(player)
            && this.logger.log(`win('${player}')`);
    }
    
    end() {
        return super.end()
            && this.logger.log(`end()`);
    }

}
