class Action {

    constructor(game) {
        this.game = game;
    }

    isAllowed() { return false; }

    isValid(element) { return false; }

    execute(element) { return this; }

    isDone() { return true; }

    toString() { return this.constructor.name.toLowerCase(); }
}

class Build extends Action {

    constructor(game) {
        super(game);
    }

    isAllowed() {
        return this.game.board.getEnabledArrows(this.game.turn).length > 0;
    }

    isValid(arrow) {
        let src = arrow.source;
        let dst = arrow.target;
        return arrow instanceof Arrow
            && arrow.isEnabled(this.game.turn)
            && (!src.player || src.player == this.game.currentPlayer)
            && (!dst.player || dst.player == this.game.currentPlayer);
    }

    execute(arrow) {
        let src = arrow.source;
        let dst = arrow.target;
        let [newf, oldf] = (src.type == undefined) ? [src, dst] : [dst, src];
        newf = (oldf.type == "portal") ? newf.toPlatform() : newf.toPortal();
        newf.lock(this.game.turn);
        arrow.lock(this.game.turn);
        let next = new Connect(this.game, newf);
        return next.isAllowed() ? next : new Done(this.game);
    }

    isDone() { return false; }

}

class Connect extends Action {

    field;

    constructor(game, field) {
        super(game);
        this.field = field;
    }

    isAllowed() {
        return  this.field != undefined
                && this.game.board.getEnabledArrows(this.game.turn)
                   .filter((arrow) => this.isForThisField(arrow))
                   .length > 0;
    }

    isForThisField(arrow) {
        return arrow.isEnabled(this.game.turn, false)
            && (this.field.id == arrow.source.id
                || this.field.id == arrow.target.id);
    }

    isValid(arrow) {
        return arrow instanceof Arrow && this.isForThisField(arrow);
    }

    execute(arrow) {
        let src = arrow.source;
        let dst = arrow.target;
        if ((dst.locked == this.game.turn && src.locked < this.game.turn)
            || (src.locked == this.game.turn && dst.locked < this.game.turn)) {
            arrow.lock(this.game.turn);
        }
        return this.isAllowed() ? this : new Done(this.game);
    }

}

class Move extends Action {

    constructor(game) {
        super(game);
    }

    isAllowed() {
        return this.game.board.getPortals().some((p) => p.active);
    }

    isValid(field) {
        return field instanceof Portal && field.active;
    }

    execute(field) {
        field.move(this.game.currentPlayer);
        field.diminish();
        return new Done(this.game);
    }

}

class Start extends Action {

    constructor(game) {
        super(game);
    }

    isAllowed() {
        return this.game.board.getStart(this.game.currentPlayer).empty;
    }

    isValid(field) {
        return field instanceof Platform
            && field.player == this.game.currentPlayer
            && field.empty;
    }

    execute(field) {
        field.putToken(this.game.currentPlayer);
        return new Done(this.game);
    }

}

class Finish extends Action {

    constructor(game) {
        super(game);
    }

    isAllowed() {
        return this.game.board.getCenter().isOccupiedBy(this.game.currentPlayer);
    }

    isValid(field) {
        return field instanceof Center
            && field.isOccupiedBy(this.game.currentPlayer);
    }

    execute(field) {
        field.removeToken();
        let player = this.game.currentPlayer;
        this.game.board.markCenterReached(player);
        return new Done(this.game);
    }

}

class Done extends Action {

    constructor(game) {
        super(game);
    }

    isValid(element) { return true; }

}

