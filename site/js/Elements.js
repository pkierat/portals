class BoardElement {

    board;
    element;

    constructor(board, element) {
        this.board = board;
        this.element = element;
    }

    get id() { return this.element.id; }
    get locked() { return this.element.dataset.locked; }

    lock(turn) { this.element.dataset.locked = turn; }
    
    disable(turn) { this.element.dataset.disabled = turn; }

}

class Field extends BoardElement {

    constructor(board, element) {
        super(board, element);
    }

    highlight() {
        this.element.dataset.active = "1";
    }

    diminish() { delete this.element.dataset.active; }

    get type() { return this.element.dataset.type; }

    get player() { return this.element.dataset.player; }
    set player(player) { this.element.dataset.player = player; }

    get coords() { return new Coords(this.element.id); }

    arrow(dir) {
        return new Arrow(this.board, this.element.querySelector(`.arrow[data-dir='${dir}'`));
    }

    toStart(player) { return undefined; }

}

class Portal extends Field {

    constructor(board, element) {
        if (element.dataset.type != "portal") { throw "Not a portal!"; }
        super(board, element);
    }

    get active() {
        let player = this.board.game.currentPlayer;
        return this.incomingPlatforms.every((p) => p.isOccupiedBy(player, true))
            && this.outgoingPlatforms.every((p) => p.empty);
    }

    get incomingPlatforms() {
        return Array.from(this.element.querySelectorAll(".field .arrow[data-disabled]"))
            .map((a) => new Arrow(this.board, a).target);
    }
    
    get outgoingPlatforms() {
        return Array.from(this.element.querySelectorAll(".field .arrow[data-locked]"))
            .map((a) => new Arrow(this.board, a).target);
    }

    move(player) {
        if (!this.active) { return; }
        this.incomingPlatforms.forEach((p) => p.removeToken());
        this.outgoingPlatforms.forEach((p) => p.putToken(player));
    }

    highlight() {
        this.incomingPlatforms.forEach((p) => p.highlight());
        this.outgoingPlatforms.forEach((p) => p.highlight());
        super.highlight();
    }

    diminish() {
        this.incomingPlatforms.forEach((p) => p.diminish());
        this.outgoingPlatforms.forEach((p) => p.diminish());
        super.diminish();
    }

}

class Platform extends Field {

    constructor(board, element) {
        if (element.dataset.type != "platform") { throw "Not a platform!"; }
        super(board, element);
    }

    get empty() {
        return this.element.querySelector(".token") == null;
    }

	isCenter() { return false; }

    isOccupiedBy(player, allowBlack = false) {
        let token = this.element.querySelector(".token");
        return token != null
            && (token.dataset.player == player
            || (allowBlack && token.dataset.player == "black"));
    }

    removeToken() {
        this.element.querySelector(".token").remove();
    }

    putToken(player) {
        let token = this.board.newToken(player);
        token.x = this.element.children[1].getAttribute("cx");
        token.y = this.element.children[1].getAttribute("cy");
        this.element.appendChild(token.element);
    }
 
}

class Center extends Platform {

    constructor(board, element) {
        super(board, element);
    }

    isCenter() { return true; }

    isReached(player) {
        return this.element.dataset.reached.includes(player);
    }

    markReached(player) {
        if (!this.isReached(player)) {
            this.element.dataset.reached += `,${player}`;
        }
    }

}

class Token extends BoardElement {

    _player;

    constructor(board, element, player) {
        super(board, element);
        this.player = player;
    }

    get x() { return this.element.getAttribute("cx"); }
    set x(val) { this.element.setAttribute("cx", val); }
    
    get y() { return this.element.getAttribute("cy"); }
    set y(val) { this.element.setAttribute("cy", val); }

    get player() { return this._player; }
    set player(val) {
        this._player = val;
        this.element.dataset.player = val.color;
    }

}

class Undefined extends Field {

    constructor(board, element) {
        super(board, element);
    }
  
    toPlatform() {
        this.element.dataset.type = "platform";
        return new Platform(this.board, this.element);
    }

    toStart(player) {
        this.element.dataset.type = "platform";
        this.element.dataset.player = player;
        this.element.dataset.locked = 0;
        return new Platform(this.board, this.element);
    }

    toPortal() {
        this.element.dataset.type = "portal";
        return new Portal(this.board, this.element);
    }

}

class Arrow extends BoardElement {

    constructor(board, element) {
        super(board, element);
    }
   
    get dir() { return new Coords(this.element.dataset.dir); }

    get source() {
        return this.board.asField(this.element.parentNode);
    }

    get target() {
        return this.board.fieldAt(this.source.coords.translate(this.dir));
    }

    isEnabled(turn, forBuild = true) {
        let [src, dst] = [this.source, this.target];
        return (dst.type != src.type)
            && !(src.locked < turn && dst.locked < turn)
            && (forBuild || (src.type != undefined && dst.type != undefined));
    }

    lock(turn) {
        super.lock(turn);
        this.flip().disable(turn);
    }

    flip() {
        let opp = this.dir.negate();
        return this.target.arrow(opp);
    }

}

class Coords {

    constructor(value) {
        if (typeof value === 'string') {
            this.value = value.split(/[:,]/).map((x) => parseInt(x));
        } else {
            this.value = value;
        }
    }

    translate(dir) {
        return new Coords(this.value.map((x, i) => x + dir.value[i]));
    }

    negate() {
        return new Coords(this.value.map((x) => -x));
    }

    toString() {
        return this.value.join(':');
    }

}
