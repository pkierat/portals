function player(fieldId, color) {
    game.controller.player(fieldId, color);
}

function start() {
    game.controller.start();
}

function field(id) {
    game.controller.field(id);
}

function arrow(fieldId, dir) {
    game.controller.arrow(fieldId, dir);
}

function end_turn() {
    game.controller.end_turn();
}

function turn(player, action) {
    game.controller.turn(player, action);
}

function reset() {
    game.controller.reset();
}

function end() {
    game.controller.end();
}
