class Chat {

    messagesElement;
    newMessageElement;
    colors = {
        red:    "🟥",
        orange: "🟧",
        yellow: "🟨",
        green:  "🟩",
        blue:   "🟦",
        violet: "🟪"
    };

    constructor(chatElement, promptElement, handler) {
        this.messagesElement = chatElement.querySelector("#messages");
        this.newMessageElement = promptElement.querySelector("#message");

        promptElement.onsubmit = (e) => handler.send(e);
    }

    message(timestamp, sender, text) {
        if (text != "") {
            let time = new Date(timestamp).toLocaleTimeString();
            let color = sender == undefined ? "⬛": this.colors[sender];
            this.messagesElement.value += `[${time}][${color}] ${text}\n`;
            this.messagesElement.scrollTop = this.messagesElement.scrollHeight;
        }
    }

}
