class ControlEventHandler {

    endTurn() {
        if (!game.isLocalPlayersTurn()) { return; }
        game.onTurnEnd();
    }
    
    resetTurn() {
        if (!game.isLocalPlayersTurn()) { return; }
        game.onTurnReset();
    }

}

class BoardEventHandler {

    onFieldClick(el) {
        if (!game.isLocalPlayersTurn()) { return; }
        let field = game.board.asField(el);
        game.onFieldClick(field);
    }
    
    onArrowClick(el) {
        if (!game.isLocalPlayersTurn()) { return; }
        let arrow = game.board.asArrow(el);
        game.onArrowClick(arrow);
    }
    
    onFieldOver(el) {
        if (!game.isLocalPlayersTurn()) { return; }
        if (el.dataset.type != "portal") { return; }
        let field = game.board.asField(el);
        if (field.active) {
            field.highlight();
        }
    }
    
    onFieldOut(el) {
        if (!game.isLocalPlayersTurn()) { return; }
        if (el.dataset.type != "portal") { return; }
        let field = game.board.asField(el);
        field.diminish();
    }

}

class ConfigEventHandler {

    startLocal() {
        game.start();
        game.nextTurn();
        return false;
    }

    startOnline() {
        let players = game.board.getPlayers().map(p => `${p.color}=${p.start}`).join('&');
        let form = document.getElementById("start");
        form.querySelector('input[name="players"]').value = players;
        return true;
    }
    
}

class ChatEventHandler {

    send(evt) {
        evt.preventDefault();
        let text = game.chat.newMessageElement.value;
        game.controller.message(text);
        game.chat.newMessageElement.value = '';
        return false;
    }

}

class EventLogger {

    log(evt) {
        console.info(evt.padEnd(32) + '//');
        return true;
    }

}

