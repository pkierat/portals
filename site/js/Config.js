class Config {
   
    options;
    eventSource;
 
    constructor(element, handler) {
        this.element = element;
        this.element.querySelectorAll("#start_local").forEach((button) => {
            button.onclick = (e) => handler.startLocal();
        });
        this.element.querySelectorAll("#start_online").forEach((button) => {
            button.onclick = (e) => handler.startOnline();
        });
        
        let opts = {};
        window.location.search.substring(1).split('&').forEach(
            (kv) => { opts[kv.split('=')[0]] = kv.split('=')[1]; }
        );
        this.options = opts;
    }

    get online() {
        return this.options.table != undefined;
    }

    get viewOnly() {
        return this.online && this.options.player != undefined;
    }

    enable(enabled) {
        this.element.querySelectorAll("input, button").forEach((el) => { el.disabled = !enabled });
    }

}
