class Dice {

    constructor(element) {
        this.element = element;
    }

    get action() { return this.element.querySelector("#roll-action").textContent; }
    set action(action) {
        this.element.querySelector("#roll-action").textContent = action;
    }

    roll(player) {
        let roll = Math.floor((Math.random()*8)) + 1;
        this.element.dataset.player = player;
        this.element.dataset.roll = roll;
        this.element.querySelector("#roll-result").textContent = roll;
        switch (roll) {
            case 1:  case 2: case 3: return Action.BUILD;
            case 4:  case 5: case 6: return Action.MOVE;
            case 7:  return Action.START;
            default: return Action.FINISH;
        }
    }

}

class Action {

    static BUILD = new Action('BUILD');
    static MOVE = new Action('MOVE');
    static START = new Action('START');
    static FINISH = new Action('FINISH');

    constructor(name) {
        this.name = name;
    }

    toString() {
        return this.name;
    }

}
