class Board {

    static CENTER = "0:0:0";

    constructor(element, handler) {
        this.element = element;
        this.handler = handler;
        this.setEventHandlers();
    }

    get player() { return this.element.dataset.player; }
    set player(p) { this.element.dataset.player = p; }

    fieldAt(coords) {
        let el = this.element.getElementById(coords);
        return this.asField(el);
    }

    asField(el) {
        switch (el.dataset.type) {
            case "portal"  : return new Portal(this, el);
            case "platform": return el.id == Board.CENTER
                                ? new Center(this, el)
                                : new Platform(this, el);
            default : return new Undefined(this, el);
        }
    }

    asArrow(el) { return new Arrow(this, el); }

    newToken(player) {
       return new Token(this, this.element.querySelector("#token").cloneNode(), player); 
    }

    save() {
        this.last = this.element.cloneNode(true);
    }

    restore() {
        let newBoard = this.last.cloneNode(true);
        this.element.replaceWith(newBoard);
        this.element = newBoard;
        this.setEventHandlers();
    }

    hasToken(player) {
        return this.element.querySelector(`.field .token[data-player='${player}']`) != null;
    }

    isCenterReached(player) {
        return this.getCenter().isReached(player);
    }

    markCenterReached(player) {
        this.getCenter().markReached(player);
    }

    getCenter() {
        return new Center(this, this.element.querySelector(`.field[id='${Board.CENTER}']`));
    }

    getStart(player) {
        return new Platform(this, this.element.querySelector(`.field[data-player='${player}']`));
    }

    getPortals() {
        return Array
            .from(this.element.querySelectorAll(".field[data-type='portal']"))
            .map((el) => new Portal(this, el));
    }

    getEnabledArrows(turn) {
        let arrows = this.element
            .querySelectorAll(".field .arrow:not([data-locked]):not([data-disabled])");
        return Array.from(arrows)
            .map((el) => new Arrow(this, el))
            .filter((arrow) => arrow.isEnabled(turn));
    }

    getPlayers() {
        return Array.from(this.element.querySelectorAll(".field[data-player]"))
            .map((el) => { return new Player(el); });
    }

    setEventHandlers() {
	    this.element.querySelectorAll(".field").forEach((el) => {
            el.onclick = (e) => { this.handler.onFieldClick(e.currentTarget); e.stopPropagation(); };
            el.onmouseover = (e) => this.handler.onFieldOver(e.currentTarget);
            el.onmouseout = (e) => this.handler.onFieldOut(e.currentTarget);
	    });
        this.element.querySelectorAll(".arrow").forEach((el) => {
            el.onclick = (e) => { this.handler.onArrowClick(e.currentTarget); e.stopPropagation(); };
	    });
    }

}
