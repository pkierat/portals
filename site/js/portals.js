window.addEventListener('load', () => {
    let frame = document.getElementById("_board_");
    frame.before(frame.contentDocument.children[0]);
    frame.remove();

    let boardElement = document.getElementById("board");
    let configElement = document.getElementById("config");
    let controlsElement = document.getElementById("controls");
    let chatElement = document.getElementById("chat");
    let promptElement = document.getElementById("prompt");
    let gameElement = document.getElementById("game");

    let board = new Board(boardElement, new BoardEventHandler());
    let config = new Config(configElement, new ConfigEventHandler());
    let ui = new UI(gameElement, controlsElement, new ControlEventHandler());
    let chat = new Chat(chatElement, promptElement, new ChatEventHandler());
    game = new Game(gameElement, board, config, ui, chat, new EventLogger());
}, false);
