class Setup extends State {

    players = ["red", "orange", "yellow", "green", "blue", "violet"];

    constructor(game) {
        super(game);
    }

    toString() { return "setup"; }

    onFieldClick(field, execute) {
        if (this.players.length == 0) { return false; }
        if (execute) {
            if (field.player != undefined) {
                this.players.push(field.player);
                field.player = this.players.shift();
            } else {
                field.toStart(this.players.shift());
                this.game.enableStart();
            }
        }
        return true;
    }

}
