class State {

    constructor(game) {
        this.game = game;
    }

    onGameStart() { return false; }

    onTurnStart(action = undefined) { return false; }

    onFieldClick(field, execute) { return false; }

    onArrowClick(arrow, execute) { return false; }

    onTurnReset(execute) { return false; }

    onTurnEnd(execute) { return false; }

    onGameEnd() { return false; }

}
