class StateDecorator {

    state;

    constructor(game, state) {
        this.game = game;
        this.state = state;
    }

    onGameStart() { return this.state.onGameStart(); }

    onTurnStart(action) { return this.state.onTurnStart(action); }

    onFieldClick(field) { return this.state.onFieldClick(field); }

    onArrowClick(arrow) { return this.state.onArrowClick(arrow); }

    onTurnReset() { return this.state.onTurnReset(); }

    onTurnEnd() { return this.state.onTurnEnd(); }

    onGameEnd() { return this.state.onGameEnd(); }

    toString() { return this.state.toString(); }

}


