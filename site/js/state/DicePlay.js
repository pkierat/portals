class DicePlay extends State {

    constructor(game) {
        super(game);
    }

    toString() { return "dice-play"; }
    
    onTurnStart(action = undefined) {
        if (action != undefined) { this.setAction(action); return true; }
        let roll = Math.floor((Math.random()*8)) + 1;
        switch (roll) {
            case 8:
                action = new Finish(this.game);
                if (action.isAllowed()) { break; }
            case 7: action = new Start(this.game); break;
            case 6: case 5: case 4: action = new Move(this.game); break;
            default: action = new Build(this.game);
        }
        alert("Action: " + action.toString().toUpperCase());
        if (!action.isAllowed()) {
            action = new Done(this.game);
            return false;
        }
        return super.onTurnStart(action);
    }

    onTurnReset(execute) { return true; }

}


