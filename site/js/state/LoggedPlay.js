class LoggedPlay extends StateDecorator {
    
    logger;

    constructor(game, state, logger) {
        super(game, state);
        this.logger = logger;
    }

    onGameStart() {
        this.game.players.forEach((p) => this.game.logger.log(`player("${p.start}", "${p.color}")`));
        this.log(`start()`);
        super.onGameStart();
    }

    onTurnStart(action = undefined) {
        super.onTurnStart(action);
        this.log(`turn("${this.game.current}")`); //TODO support action from dice
    }

    onArrowClick(arrow) {
        if (super.onArrowClick(arrow)) {
            this.log(`arrow("${arrow.source.id}", "${arrow.dir}")`);
            return true;
        }
        return false;
    }
    
    onFieldClick(field) {
        if (super.onFieldClick(field)) {
            this.log(`field("${field.id}")`);
            return true;
        }
        return false;
    }

    onTurnReset() {
        if (super.onTurnReset()) {
            this.log(`reset()`);
            return true;
        }
        return false;
    }

    onGameEnd() {
        super.onGameEnd();
        this.log("end()");
    }

    log(message) {
        this.logger.log(message);
    }

}


