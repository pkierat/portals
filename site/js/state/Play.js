class Play extends State {

    action;

    constructor(game) {
        super(game);
        this.action = undefined;
    }

    toString() { return "free-play"; }

    onGameStart() { return true; }

    onTurnStart(action) {
        if (action != undefined) {
            return this.setAction(action);
        }
        let finish = new Finish(this.game);
        if (finish.isAllowed()) {
            return this.setAction(finish);
        }
        return new Start(this.game).isAllowed()
            || new Move(this.game).isAllowed()
            || new Build(this.game).isAllowed()
            || this.setAction(new Done(this.game));
    }

    setAction(action) {
        let allowed = action.isAllowed();
        if (!allowed) { action = new Done(this.game); }
        this.action = action;
        this.game.action = action;
        return allowed;
    }

    onArrowClick(arrow, execute) {
        let action = this.action;
        if (action == undefined) {
            action = new Build(this.game);
        }
        if (!action.isValid(arrow)) { return false; }
        if (execute) {
            this.setAction(action.execute(arrow));
        }
        return true;
    }

    onFieldClick(field, execute) {
        let action = this.action;
        if (this.action == undefined) {
            action = new Start(this.game);
            if (!action.isValid(field)) {
                action = new Move(this.game);
            }
        }
        if (!action.isValid(field)) { return false; }
        if (execute) {
            this.setAction(action.execute(field));
        }
        return true;
    }

    onTurnReset(execute) {
        if (execute) {
            this.action = undefined;
            this.game.action = undefined;
        }
        return true;
    }

    onTurnEnd(execute) {
        if (!this.action || !this.action.isDone()) { return false; }
        if (execute) {
            this.onTurnReset(true);
        }
        return true;
    }
}


