ADOC_OPTS := --failure-level WARN -r asciidoctor-diagram

sources  := $(wildcard *.adoc)
images   := $(wildcard img/*.png)
targets  := $(patsubst %.adoc,%,$(sources))

.PHONY: clean deps all pdf html

all: pdf

pdf: $(targets:=.pdf)

html: $(targets:=.html)

$(targets): %: %.pdf

%.pdf: %.adoc $(images) theme.yml
	asciidoctor-pdf $(ADOC_OPTS) -o $@ $<
#	pdfjam --nup 1x2,landscape -o $(patsubst %.pdf,%-print.pdf,$@) -- $@ 2,1,3,4,12,5,6,11,10,7,8,9

%.html: %.adoc $(images)
	asciidoctor $(ADOC_OPTS) -o $@ $<

deps:
	sudo apt-get -y install ruby
	sudo gem install --pre asciidoctor asciidoctor-pdf asciidoctor-diagram pygments.rb

clean:
	rm -rf *.pdf *.html *.aux *.fdb_latexmk *.fls *.vrb *.out *.log *.nav *.snm *.toc

